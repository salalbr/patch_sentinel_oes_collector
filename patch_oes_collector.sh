#!/bin/bash
##############################################################################
#  Bug 688485 - Patching script
#  Authors:
#              Maer Melo | mmelo@novell.com
##############################################################################

banner() {
	echo "============================================================================="
	echo "                     Bug 688485 - Patching script"
	echo -e "=============================================================================\n"
}

usage() {
	banner
	echo -e "Usage: ./patch-688485.sh\n"
}

OES_LEVEL=`cat /etc/SuSE-release | grep PATCHLEVEL | sed "s/^PATCHLEVEL = //;s/\"//g"`
CURRENT_PATH=`pwd`


## Test call parameters
if [ $# -ne 0 ]
then
	usage
	exit 2
fi

## Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi


if [ $OES_LEVEL = "3" ]
then
	## Stop vlog
	echo -e "Shutting down sentagent\n"
	killproc -TERM /usr/local/sbin/sentagent
	sleep 10

	## Stop vigil
        if ps ax | grep cifsd | grep -v grep >/dev/null 2>&1; then
            echo -n "Notifying CIFS... "
            if ! novcifs -t no; then
                echo "Error:  CIFS notification failed."
            fi
        fi
        if ps ax | grep ndsd | grep -v grep >/dev/null 2>&1; then
            echo -n "Notifying NCP... "
            if ! ncpcon set AUDITING_SUPPORT=0 >/dev/null 2>&1; then
                echo "Error:  NCP notification failed."
            fi
        fi
        echo -n "Removing modules... "
        if rmmod -s vigil_cifs && \
           rmmod -s vigil_nss && \
           rmmod -s vigil_ncp && \
           rmmod -s vigil && \
           rmmod -s pmd; then
                echo "vigil modules removed."
        else
           echo -n "Unable to remove one or more vigil modules."
        fi

	## Backup vlog and vigil modules
	md5sum /opt/novell/lib64/libvigil.so.* /opt/novell/vigil/bin/vlog >>temp.log
	ls -la /opt/novell/lib64/libvigil.so.* /opt/novell/vigil/bin/vlog >>temp.log
	zip "backup-patch-bug688485-`date '+%m-%d-%Y-%H-%M'`.zip" /opt/novell/lib64/libvigil.so.* /opt/novell/vigil/bin/vlog temp.log
	rm -rf temp.log

	## Apply patch
	rm -rf /opt/novell/lib64/libvigil.so.0.1.1 /opt/novell/lib64/libvigil.so.0
	rm -rf /opt/novell/vigil/bin/vlog
	cp libvigil.so.0.0.1 /opt/novell/lib64/libvigil.so.0.0.1
	cp vlog /opt/novell/vigil/bin/vlog
	chown root.root /opt/novell/lib64/libvigil.so.0.0.1 /opt/novell/vigil/bin/vlog
	chmod +x /opt/novell/vigil/bin/vlog
	chmod 644 /opt/novell/lib64/libvigil.so.0.0.1
	cd /opt/novell/lib64/
	ln -s libvigil.so.0.0.1 libvigil.so.0
	cd $CURRENT_PATH

	##Starting vigil modules
	/etc/init.d/novell-vigil start
	sleep 10

	##Starting vlog
	/etc/init.d/sentagent start
		
fi

exit 0
